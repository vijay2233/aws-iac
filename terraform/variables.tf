# Defining Region
variable "aws_region" {
  default = "us-east-1"
}

variable "key_name" {
    type = string
    default = "Laptop"
}

# Defining CIDR Block for VPC
# Showing demo
variable "vpc_cidr" {
  default = "10.0.0.0/16"
}
